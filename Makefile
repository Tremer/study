CFLAGS = -Wall -Werror -Isrc

geom:	obj/main.o lib/libgeom.a
	gcc $(CFLAGS) obj/main.o lib/libgeom.a -o bin/geom -lm

obj/main.o: src/geom/main.c
	gcc -c $(CFLAGS) src/geom/main.c -o obj/main.o

lib/libgeom.a: obj/geom.o
	ar rcs lib/libgeom.a obj/geom.o

obj/geom.o: src/geom/geom.c
	gcc -c $(CFLAGS) src/geom/geom.c -o obj/geom.o

clean:
	rm -rf obj/*.o  bin/geom lib/*.a

